alias Vid.Accounts.User

defmodule VidWeb.PageController do
  use VidWeb, :controller

  @spec index(Plug.Conn.t(), any()) :: Plug.Conn.t()
  def index(conn, _params) do
    user = %User{} #Repo.get_by!(User, name: "mikelat")
    render(conn, "index.html", user: user)
  end
end
