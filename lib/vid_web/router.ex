defmodule VidWeb.Router do
  use VidWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug :put_layout, false
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/api", VidWeb do
    pipe_through :api

    # resources "/users", UserController
    # resources "/sessions", SessionController

    # forward "/graphiql", Absinthe.Plug.GraphiQL, schema: VidWeb.Schema

    forward "/", Absinthe.Plug, schema: BlogWeb.Schema

  end

  scope "/" do
    pipe_through :api

    if Mix.env == :dev do
      forward "/graphiql", Absinthe.Plug.GraphiQL,
        schema: VidWeb.Schema,
        interface: :simple,
        context: %{pubsub: VidWeb.Endpoint}
    end
  end

  scope "/", VidWeb do
    pipe_through :browser

    # get "/", PageController, :index
    get "/*path", PageController, :index
  end

  # Other scopes may use custom stacks.
  # scope "/api", VidWeb do
  #   pipe_through :api
  # end
end
