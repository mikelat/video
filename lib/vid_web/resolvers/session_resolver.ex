defmodule VidWeb.NewsResolver do
  alias Vid.News

  def all_links(_root, _args, _info) do
    links = News.list_links()
    {:ok, links}
  end

  def create_session(_root, args, _info) do
    case News.create_session(args) do
      {:ok, session} -> {:ok, session}
      _error -> {:error, "could not create session"}
    end
  end
end
