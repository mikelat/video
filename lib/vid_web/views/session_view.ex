defmodule VidWeb.SessionView do
  use VidWeb, :view
  alias VidWeb.SessionView

  def render("show.json", %{session: session}) do
    %{data: render_one(session, SessionView, "session.json")}
  end

  def render("session.json", %{session: session}) do
    %{id: session.id,
      user: session.user}
  end
end
