defmodule VidWeb.PageView do
  use VidWeb, :view
  alias VidWeb.UserView

  def render("index.html", assigns) do
    raw """
<head>
  <meta charset="utf-8"/>
  <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  #{csrf_meta_tag() |> safe_to_string()}
  <link rel="stylesheet" href="#{Routes.static_path(assigns.conn, "/css/app.css")}" />
</head>
<body>
  <div id="app" />
  <script>
    globalState = { user: #{user_json(assigns.user)} }
  </script>
  <script src="#{Routes.static_path(assigns.conn, "/js/app.js")}"></script>
</body>
"""
  end

  defp user_json(user) do
    user
    |> render_one(UserView, "user.json")
    |> Jason.encode!()
  end
end
