defmodule VidWeb.Schema do
  use Absinthe.Schema

  alias VidWeb.NewsResolver
  alias VidWeb.SessionResolver

  object :link do
    field :id, non_null(:id)
    field :url, :string
    field :description, :string
    field :created_at, :string
  end

  query do

    field :all_links, non_null(list_of(non_null(:link))) do
      resolve &NewsResolver.all_links/3
    end
    #field :all_links, non_null(list_of(non_null(:link)))
  end

  mutation do
    field :create_link, :link do
      arg :url, non_null(:string)
      arg :description, non_null(:string)

      resolve &NewsResolver.create_link/3
    end

    # field :create_session, :session do
    #   arg :email, non_null(:string)
    #   arg :password, non_null(:string)

    #   resolve &SessionResolver.create_session/2
    # end
  end


end
