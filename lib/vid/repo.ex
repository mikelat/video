defmodule Vid.Repo do
  use Ecto.Repo,
    otp_app: :vid,
    adapter: Ecto.Adapters.Postgres
end
