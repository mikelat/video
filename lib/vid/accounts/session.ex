defmodule Vid.Accounts.Session do
  use Vid.Schema
  import Ecto.Changeset

  schema "sessions" do
    field :user, Ecto.UUID

    timestamps()
  end

  @doc false
  def changeset(session, attrs) do
    session
    |> cast(attrs, [:user])
    |> validate_required([:user])
  end
end
