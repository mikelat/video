defmodule Vid.Accounts.User do
  use Vid.Schema
  import Ecto.Changeset

  schema "users" do
    field :email, :string
    field :name, :string
    field :password, :string, virtual: true
    field :password_encrypted, :string

    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:email, :name, :password])
    |> validate_required([:email, :name, :password])
    |> unique_constraint(:email)
    |> set_password
  end

  defp set_password(changeset) do
    password = get_change(changeset, :password)
    if changeset.valid? && !is_nil(password) do
      force_change(changeset, :password_encrypted, Argon2.hash_pwd_salt(password))
    else
      changeset
    end
  end
end
