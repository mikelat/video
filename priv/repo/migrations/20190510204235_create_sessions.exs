defmodule Vid.Repo.Migrations.CreateSessions do
  use Ecto.Migration

  def change do
    create table(:sessions) do
      add :user, :uuid

      timestamps()
    end

  end
end
