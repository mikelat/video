import * as React from 'react'

class ErrorBoundary extends React.Component {
  constructor(props) {
    super(props)
    this.state = { hasError: false, error: {} }
  }

  static getDerivedStateFromError(error) {
    return { hasError: true, error: error }
  }

  render() {
    if (this.state.hasError) {
      return (
        <div className="container text-danger pt-2 lead">
          <img
            src="/images/science.png"
            width="300px"
            className="rounded float-left pr-2"
          />
          OOPSIE WOOPSIE!! Uwu We make a fucky wucky!! A wittle fucko boingo!
          The code monkeys at our headquarters are working VEWY HAWD to fix
          this!
          <br />
          <br />
          {this.state.error.message}
        </div>
      )
    }
    return this.props.children
  }
}

export default ErrorBoundary
