import Main from 'components/Main'
import * as React from 'react'
import { useTranslation } from 'react-i18next'
import { Link } from 'react-router-dom'
import { Jumbotron } from 'react-bootstrap'

const IndexPage = () => {
  const { t } = useTranslation()
  return (
    <Main>
      <Jumbotron fluid>
        <p>{t('welcome-msg')}</p>
      </Jumbotron>
      <Link to="/fetch-data">Fetch Data</Link>
      <br />
      <Link to="/counter">Counter</Link>
    </Main>
  )
}

export default IndexPage
