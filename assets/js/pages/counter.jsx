import React, { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'

import Main from 'components/Main'
import { setWindowTitle } from 'lib/functions'

const CounterPage = () => {
  const [counter, setCounter] = useState(0)
  useEffect(() => setWindowTitle('test'))

  return (
    <Main>
      <h1>Counter</h1>
      <p>
        The Counter is the simplest example of what you can do with a React
        component.
      </p>
      <p>
        Current count: <strong>{counter}</strong>
      </p>
      <button className="button" onClick={() => setCounter(counter + 1)}>
        Increment counter
      </button>{' '}
      <button
        className="button button-outline"
        onClick={() => setCounter(counter - 1)}
      >
        Decrement counter
      </button>{' '}
      <button className="button button-clear" onClick={() => setCounter(0)}>
        Reset counter
      </button>
      <br />
      <br />
      <p>
        <Link to="/">Back to home</Link>
      </p>
    </Main>
  )
}

export default CounterPage
