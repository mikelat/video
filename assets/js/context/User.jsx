import React, { useState } from 'react'

const [UserContext, UserProvider] = [
  React.createContext([{}, () => {}]),
  ({ children }) => {
    const [state, setState] = useState({})
    return (
      <UserContext.Provider value={[state, setState]}>
        {children}
      </UserContext.Provider>
    )
  }
]

export { UserContext, UserProvider }
