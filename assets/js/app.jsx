import React, { Suspense } from 'react'
import * as ReactDOM from 'react-dom'
import { BrowserRouter, Route, Switch } from 'react-router-dom'

import Header from 'components/Header'

import 'font-awesome/css/font-awesome.min.css'
import '../css/app.sass'

import 'lib/i18n'
import 'phoenix_html'

import HomePage from 'pages'
import { UserProvider } from 'context/User'
import CounterPage from 'pages/counter'
// import FetchDataPage from "pages/fetch-data";

import ErrorBoundary from 'pages/error-boundary'

const App = () => (
  <UserProvider>
    <Suspense fallback={<div>Loading...</div>}>
      <BrowserRouter>
        <Header />
        <ErrorBoundary>
          <Switch>
            <Route exact path="/" component={HomePage} />
            <Route path="/counter" component={CounterPage} />
          </Switch>
        </ErrorBoundary>
      </BrowserRouter>
    </Suspense>
  </UserProvider>
)

ReactDOM.render(<App />, document.getElementById('app'))
