import i18n from 'i18next'
import en from 'locale/en'
import { initReactI18next } from 'react-i18next'

// import LanguageDetector from "i18next-browser-languagedetector";
// import Backend from "i18next-xhr-backend";

export default i18n
  // .use(Backend)
  // .use(LanguageDetector)
  .use(initReactI18next)
  .init({
    debug: true,
    fallbackLng: 'en',
    lng: 'en',
    lowerCaseLng: true,
    // react: {
    //   useSuspense: false,
    // },
    resources: {
      en
    },

    interpolation: {
      escapeValue: false // not needed for react as it escapes by default
    }
  })
