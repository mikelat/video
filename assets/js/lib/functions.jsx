import i18n from 'i18next'

export function setWindowTitle(title) {
  document.title = i18n.t('title', { title })
}
