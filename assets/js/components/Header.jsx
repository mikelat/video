import React, { useState, useContext, useCallback } from 'react'
import { Image } from 'react-bootstrap'
import { NavLink } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import { UserContext } from 'context/User'
import md5 from 'js-md5'

import SignInRegisterModal from './user/SignInRegisterModal'

const Header = () => {
  const { t } = useTranslation()

  const [signInModal, setSignInModal] = useState(false)
  const user = useContext(UserContext)

  const closeModal = useCallback(() => setSignInModal(false), [])
  const signInClick = useCallback(event => {
    event.preventDefault()
    setSignInModal(true)
  }, [])

  return (
    <header id="header" className="flex-shrink-0 bg-dark">
      <div className="container">
        <nav className="navbar navbar-expand-lg navbar-dark">
          <NavLink to="/" className="navbar-brand">
            <i className="fa fa-film mr-1" aria-hidden="true"></i>
            Latube
          </NavLink>
          <ul className="navbar-nav mr-auto">
            <li className="nav-item"></li>
            <li className="nav-item">
              <a className="nav-link" href="#">
                Link
              </a>
            </li>
          </ul>
          {user.id ? (
            <NavLink to={`/u/${user.name}`}>
              {user.name}
              <Image
                className="ml-2"
                src={`https://www.gravatar.com/avatar/${md5(user.email)}?s=30`}
                rounded
              />
            </NavLink>
          ) : (
            <a className="nav-link" href="#" onClick={signInClick}>
              {t('header.sign-in')}
            </a>
          )}
        </nav>
      </div>
      {signInModal && <SignInRegisterModal closeModal={closeModal} />}
    </header>
  )
}

export default Header
