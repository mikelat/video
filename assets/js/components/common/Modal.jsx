import React, { useState, useCallback } from 'react'
import PropTypes from 'prop-types'
import { useTranslation } from 'react-i18next'

const [propTypes, SignInRegisterModal] = [
  {
    closeModal: PropTypes.func.isRequired,
    showRegistrationModal: PropTypes.bool
  },
  ({ closeModal, showRegistrationModal = false }) => {
  }
  ]