import * as React from 'react'
import PropTypes from 'prop-types'

const [propTypes, Main] = [
  {
    children: PropTypes.any.isRequired
  },
  ({ children }) => (
    <main role="main" className="flex-shrink-0">
      <div className="container">{children}</div>
    </main>
  )
]

Main.propTypes = propTypes
export default Main
