import React, { useState, useCallback } from 'react'
import PropTypes from 'prop-types'
import { Button, Form, Modal } from 'react-bootstrap'
import { useTranslation } from 'react-i18next'

const [propTypes, SignInRegisterModal] = [
  {
    closeModal: PropTypes.func.isRequired,
    showRegistrationModal: PropTypes.bool
  },
  ({ closeModal, showRegistrationModal = false }) => {
    const { t } = useTranslation()

    const [isRegister, setIsRegister] = useState(showRegistrationModal)
    const [form, setForm] = useState({ name: 'asdasdasd', password: 'aaaaaaaaaa', email: 'a@a.com' })
    const [validated, setValidated] = useState(false)

    const swapModal = useCallback(event => {
      event.preventDefault()
      setIsRegister(register => !register)
    }, [])

    const onSubmit = useCallback(event => {
      console.log('test')
      if (event.currentTarget.checkValidity() === false) {
        event.preventDefault()
        event.stopPropagation()
      } else {
        fetch('/test', {method: 'POST', body: JSON.stringify(form)})
      }

      setValidated(true)
    }, [form])

    const changeInput = useCallback(({target}) =>
      setForm(f => ({...f, [target.name]: target.value}))
    , [])

    return (
      <Modal show={true} onHide={closeModal}>
        <Form onSubmit={onSubmit} noValidate validated={validated}>
          <Modal.Header closeButton>
            <Modal.Title>
              {t(`account.${isRegister ? 'register' : 'sign-in'}`)}
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {isRegister && (
              <Form.Group controlId="userName">
                <Form.Control
                  type="name"
                  name="name"
                  placeholder={t('account.name')}
                  pattern=".{3,}"
                  value={form.name}
                  onChange={changeInput}
                  required
                />
              </Form.Group>
            )}
            <Form.Group controlId="userEmail">
              <Form.Control
                type="email"
                name="email"
                placeholder={t('account.email')}
                value={form.email}
                onChange={changeInput}
                pattern="[^@\s]+@[^@\s]+\.[^@\s]+"
                required
              />
            </Form.Group>
            <Form.Group controlId="userPassword">
              <Form.Control
                type="password"
                name="password"
                placeholder={t('account.password')}
                pattern=".{6,}"
                value={form.password}
                onChange={changeInput}
                required
              />
            </Form.Group>
          </Modal.Body>
          <Modal.Footer>
            <div className="col">
              <a href="#" onClick={swapModal}>
                {t(`account.${isRegister ? 'sign-in' : 'register'}-link`)}
              </a>
            </div>
            <Button variant="secondary" onClick={closeModal}>
              {t('common.close')}
            </Button>
            <Button variant="primary" onClick={onSubmit}>
              {t(`account.${isRegister ? 'register' : 'sign-in'}`)}
            </Button>
          </Modal.Footer>
        </Form>
      </Modal>
    )
  }
]

SignInRegisterModal.propTypes = propTypes
export default SignInRegisterModal
