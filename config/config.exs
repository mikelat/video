# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :vid,
  ecto_repos: [Vid.Repo]

# Configures the endpoint
config :vid, VidWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "yO+tJbAi93JjIn3p7HqcgrCsWcGU96B1CniS+I4GOOFmQxNv7u0Ya8QkvvBAZ9+B",
  render_errors: [view: VidWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Vid.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

config :vid, Vid.Repo,
  migration_primary_key: [
    name: :id,
    type: :uuid,
    autogenerate: true,
    read_after_writes: true,
    default: {:fragment, "uuid_generate_v4()"}
  ],
  migration_timestamps: [
    type: :timestamptz,
    read_after_writes: true,
    default: {:fragment, "now()"},
    inserted_at: :created_at

  ]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
